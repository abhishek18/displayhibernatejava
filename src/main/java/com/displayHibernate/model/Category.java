package com.displayHibernate.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Transactional
@Entity
@Table(name = "category")
@Proxy(lazy=false)
public class Category implements Serializable {
	
	private static final long serialVersionUID = 7025606795746255830L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id")
		private int categoryId;
	@Column(name = "catgory_desc")
		private String categoryDesc;
	
	@OneToMany(cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name = "category_id")
	//@JsonManagedReference
	 private List<SubCategory> subCatgList = new ArrayList<>();

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryDesc() {
		return categoryDesc;
	}

	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}

	public List<SubCategory> getSubCatgList() {
		return subCatgList;
	}

	public void setSubCatgList(List<SubCategory> subCatgList) {
		this.subCatgList = subCatgList;
	}
	 
	 
	
}
