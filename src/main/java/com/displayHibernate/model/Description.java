package com.displayHibernate.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/*@Transactional
@Entity
@Table(name = "description_info")
@Proxy(lazy=false)*/
public class Description implements Serializable{

	/*private static final long serialVersionUID = 7029806795746254330L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "desc_id")
	private int descId;
	
	@Column(name = "arrangement")
	private String arrangement;
	
	 @ManyToMany(mappedBy = "descList")
	 @JsonBackReference
	 private List<ItemInfo> itemList = new ArrayList<>() ;
	 
	 @OneToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	 @JoinColumn(name = "description_desc_id")
	 @JsonManagedReference
	 private List<Tag> tagList = new ArrayList<>() ;

	public int getDescId() {
		return descId;
	}

	public void setDescId(int descId) {
		this.descId = descId;
	}

	public String getArrangement() {
		return arrangement;
	}

	public void setArrangement(String arrangement) {
		this.arrangement = arrangement;
	}

	public List<ItemInfo> getItemList() {
		return itemList;
	}

	public void setItemList(List<ItemInfo> itemList) {
		this.itemList = itemList;
	}

	public List<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}*/

	
}
