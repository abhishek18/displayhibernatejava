package com.displayHibernate.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Transactional
@Entity
@Table(name = "theme")
@Proxy(lazy=false)
public class Theme implements Serializable {

	private static final long serialVersionUID = 7029808795746254330L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "theme_id")
	private int themeId;
	
	@Column(name = "theme_name")
	private String themeName;	
	@Column(name = "theme_desc")
	private String themeDesc;
	
	@ManyToMany(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinTable(name = "item_theme", joinColumns = { @JoinColumn(name = "theme_id") }, 
	inverseJoinColumns = {@JoinColumn(name = "item_id") })
	//@JsonManagedReference
	//@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="itemId")
	private List<Item> itemList = new ArrayList<>() ;
	
	@ManyToMany(mappedBy = "themeSubCList")
	@JsonBackReference
	private List<SubCategory> subcatgThemeList = new ArrayList<>() ;

	
	public int getThemeId() {
		return themeId;
	}

	public void setThemeId(int themeId) {
		this.themeId = themeId;
	}

	public String getThemeDesc() {
		return themeDesc;
	}

	public void setThemeDesc(String themeDesc) {
		this.themeDesc = themeDesc;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}
	/*public List<SubCategory> getSubcatgThemeList() {
		return subcatgThemeList;
	}

	public void setSubcatgThemeList(List<SubCategory> subcatgThemeList) {
		this.subcatgThemeList = subcatgThemeList;
	}*/

	
}
