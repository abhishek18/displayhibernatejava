package com.displayHibernate.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Transactional
@Entity
@Table(name = "sub_category")
@Proxy(lazy=false)
public class SubCategory implements Serializable {

private static final long serialVersionUID = 7025606795712255830L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "subcategory_id")
		private int subcategoryId;
	@Column(name = "subcatgory_desc")
		private String subcategoryDesc;
	
	@ManyToOne
    @JsonBackReference
	private Category category;
	
	@ManyToMany(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinTable(name = "subcategory_theme", joinColumns = { @JoinColumn(name = "subcategory_id") }, 
	inverseJoinColumns = {@JoinColumn(name = "theme_id") })
	//@JsonManagedReference
	private List<Theme> themeSubCList = new ArrayList<>() ;

	public int getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(int subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public String getSubcategoryDesc() {
		return subcategoryDesc;
	}

	public void setSubcategoryDesc(String subcategoryDesc) {
		this.subcategoryDesc = subcategoryDesc;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<Theme> getThemeSubCList() {
		return themeSubCList;
	}

	public void setThemeSubCList(List<Theme> themeSubCList) {
		this.themeSubCList = themeSubCList;
	}
	
}
