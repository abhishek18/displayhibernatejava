package com.displayHibernate.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Transactional
@Entity
@Table(name = "item")
@Proxy(lazy=false)
public class Item implements Serializable {

	private static final long serialVersionUID = 7029806795746255830L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "item_id")
		private int itemId;
	@Column(name = "item_name")
		private String itemName;			
	
	@ManyToMany(mappedBy = "itemList")
	@JsonBackReference
	//@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="themeId")
	private List<Theme> themeList = new ArrayList<>() ;

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public List<Theme> getThemeList() {
		return themeList;
	}

	public void setThemeList(List<Theme> themeList) {
		this.themeList = themeList;
	}	
	
	/*@Column(name = "group_code")
		private String groupCode;
		
	@Column(name = "upload_date")
		private Date uploadDate;
	
	@Column(name = "catalog_id")
		private String catalogId;
	@Column(name = "catalog_name")
		private String catalogName;
	@Column(name = "item_description")
		private String 	itemDescription;
	@Column(name = "item_description1")
		private String 	ItemDescription1;
	@Column(name = "item_description2")
		private String 	ItemDescription2;
	@Column(name = "item_description3")
		private String 	ItemDescription3;
	@Column(name = "item_description4")
		private String 	ItemDescription4;
	@Column(name = "fabric1")
		private String 		fabric1;
	@Column(name = "fabric2")
		private String 	fabric2;
	@Column(name = "fabric3")
		private String fabric3;
	@Column(name = "family_color")
		private String familyColor;
	@Column(name = "item_color")
		private String itemColor;
	@Column(name = "more_color")
		private String  moreColor;
	@Column(name = "primary_color")
		private String primaryColor;
	@Column(name = "look_type")
		private String LookType;
	@Column(name = "occassion")
		private String occassion;
	@Column(name = "remarks")
		private String remarks;
	@Column(name = "style")
		private String style;
	@Column(name = "sub_type")
		private String subType;
	@Column(name = "sub_work1")
		private String subWork1;
	@Column(name = "sub_work2")
		private String subWork2;
	@Column(name = "item_types")
		private String itemTypes;
	@Column(name = "item_work")
		private String itemWork;
	@Column(name = "cid_ref")
		private String cidRef;
	@Column(name = "category_id")
		private String categoryId;
	@Column(name = "age")
		private String age;
	@Column(name = "category_name")
		private String categoryName;
	@Column(name = "mrp")
		private String mrp;*/
	
	
}
