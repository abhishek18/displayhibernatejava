package com.displayHibernate.model;

import java.io.Serializable;

public class ResponseData implements Serializable{
	
	private static final long serialVersionUID = -712205974033438495L;
	private String responseStatus;
	private long responseCode;
	private String responseObj;
	private String responseUidUserType;
	private String responseMsg;
	private Object obj;
	
	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	public long getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(long responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseObj() {
		return responseObj;
	}
	public void setResponseObj(String responseObj) {
		this.responseObj = responseObj;
	}
	public String getResponseUidUserType() {
		return responseUidUserType;
	}
	public void setResponseUidUserType(String responseUidUserType) {
		this.responseUidUserType = responseUidUserType;
	}
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	
	

}
