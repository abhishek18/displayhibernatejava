package com.displayHibernate.model;

/*@Transactional
@Entity
@Table(name = "users")*/
public class User {

	/*@Id
	@Column(name = "user_id", nullable = false, length = 45)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	
	@Id
	@Column(name = "user_name", unique = true, 
		nullable = false, length = 100)*/
	private long id;
	private String username;
	
	/*@Column(name = "password", 
			nullable = false, length = 60)*/
	private String password;
	
	/*@Column(name = "enabled", nullable = false)
	private boolean enabled;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<UserRole> userRole = new HashSet<UserRole>(0);*/

	
	public User(long id, String name, String pwd){
		this.id = id;
		this.username = name;
		this.password = pwd;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/*public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}*/	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
