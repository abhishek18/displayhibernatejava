package com.displayHibernate.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.displayHibernate.dao.ImageDao;
import com.displayHibernate.model.ImageInfo;

@Service
public class ImageServiceImpl implements ImageService{

	private static final Logger logger = LoggerFactory.getLogger(ImageServiceImpl.class);
	@Autowired
	private ImageDao imageDao;
	
	/*   save an Object of Image 
	 * 
	 * @param imgObj
	 * @return Long
	 * @throws Exception
	 */
	@Transactional
	@Override
	public long saveImage(ImageInfo imgObj) throws Exception {
		logger.info("After save saveImage >> ");
		java.util.Date utilDate = new java.util.Date();
		imgObj.setCreatedOn(new java.sql.Timestamp(utilDate.getTime()));
		return imageDao.saveImage(imgObj);
	}

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@Override
	public List<ImageInfo> getImageData() throws Exception {
		return imageDao.getImageData();
	}

}
