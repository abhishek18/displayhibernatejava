package com.displayHibernate.service;

import java.util.List;

import com.displayHibernate.model.DisplayInfo;

public interface DisplayService {
	
	/*   save an Object of data 
	 * 
	 * @param dispObj
	 * @return Long
	 * @throws Exception
	 */
	public long saveDisplay(DisplayInfo dispObj) throws Exception;
	
	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	public List<DisplayInfo> getDisplayData() throws Exception;
	
	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	public DisplayInfo getDisplayDataById(int id) throws Exception;
	
	/*   Delete one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	public boolean deleteDisplayById(int id)throws Exception;
	
	/*   update the one object of data by id 
	 * 
	 * @param displayObj
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	public void updateDisplay(DisplayInfo displayObj) throws Exception;

}
