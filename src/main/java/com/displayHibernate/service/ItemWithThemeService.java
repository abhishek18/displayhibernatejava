package com.displayHibernate.service;

import java.util.List;

import com.displayHibernate.model.Item;
import com.displayHibernate.model.Theme;

public interface ItemWithThemeService {
	
	/*   save the List of item and theme data 
	 * 
	 * @param itemThemeObj
	 * @return Long
	 * @throws Exception
	 */
	public long saveItem(Theme itemThemeObj) throws Exception;

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	public List<Theme> getItemThemeData() throws Exception ;
	
	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return Item object
	 * @throws Exception
	 */
	public Item getItemDataById(int id) throws Exception;

	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return Theme object
	 * @throws Exception
	 */
	public Theme getThemeDataById(int id) throws Exception;
	
	/*   update the one object of data by id 
	 * 
	 * @param itemObj
	 * @return void
	 * @throws Exception
	 */
	public void updateItemTheme(Item itemObj) throws Exception;
	
	/*   update the one object of theme by id 
	 * 
	 * @param themeObj
	 * @return void
	 * @throws Exception
	 */
	public void updateTheme(Theme themeObj) throws Exception;
	
	/*   delete the one object of theme by id 
	 * 
	 * @param id
	 * @return void
	 * @throws Exception
	 */
	public void deleteTheme(int id) throws Exception;
	/*   Get the list of Item data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	public List<Item> getItemData() throws Exception;
}
