package com.displayHibernate.service;

import com.displayHibernate.model.User;

public interface UserService {

	public User findById(long id);
	
	public User findByName(String name);
	
	public void saveUser(User user);
	
	public boolean isUserExist(User user);
}
