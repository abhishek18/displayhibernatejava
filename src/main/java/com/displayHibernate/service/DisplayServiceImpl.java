package com.displayHibernate.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.displayHibernate.dao.DisplayDao;
import com.displayHibernate.model.DisplayInfo;

@Service
public class DisplayServiceImpl implements DisplayService{
	
	private static final Logger logger = LoggerFactory.getLogger(DisplayServiceImpl.class);
	@Autowired
	private DisplayDao displayDao;

	/*   save an Object of data 
	 * 
	 * @param
	 * @return Long
	 * @throws Exception
	 */
	@Transactional
	@Override
	public long saveDisplay(DisplayInfo dispObj) throws Exception {
		java.util.Date utilDate = new java.util.Date();
		dispObj.setCreatedOn(new java.sql.Timestamp(utilDate.getTime()));
		return displayDao.saveDisplay(dispObj);
	}

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@Override
	public List<DisplayInfo> getDisplayData() throws Exception {
		
		return displayDao.getDisplayData();
	}

	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@Override
	public DisplayInfo getDisplayDataById(int id) throws Exception {
		
		return displayDao.getDisplayDataById(id);
	}

	/*   Delete one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@Override
	public boolean deleteDisplayById(int id) throws Exception {
		logger.info("deleteDisplayById >> ");
		displayDao.deleteDisplayById(id);
		
		/*----Check whether the object has been deleted-----*/
			DisplayInfo displayObj = new DisplayInfo();
			displayObj = displayDao.getDisplayDataById(id);
			
			if(null != displayObj) {
				return false;
			}else {
				return true;
			}
		/*----Check whether the object has been deleted-----*/	
	}

	/*   update the one object of data by id 
	 * 
	 * @param displayObj
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@Override
	public void updateDisplay(DisplayInfo displayObj) throws Exception {
		logger.info("updateDisplay >> ");
		try {
			displayDao.updateDisplay(displayObj);
		}catch(Exception e) {
			logger.error("error in updateDisplay >> "+e.getMessage());
		}
		
		
	}

	

}
