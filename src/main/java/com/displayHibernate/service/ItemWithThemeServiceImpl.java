package com.displayHibernate.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.displayHibernate.dao.ItemAndThemeDao;
import com.displayHibernate.model.Item;
import com.displayHibernate.model.Theme;

@Service
public class ItemWithThemeServiceImpl implements ItemWithThemeService{

	@Autowired
	private ItemAndThemeDao itemAndThemeDao;
	private static final Logger logger = LoggerFactory.getLogger(ItemWithThemeServiceImpl.class);
	/*   save the List of item and theme data 
	 * 
	 * @param itemThemeObj
	 * @return Long
	 * @throws Exception
	 */
	@Override
	public long saveItem(Theme itemThemeObj) throws Exception {
		/*java.util.Date utilDate = new java.util.Date();
		itemThemeObj.setCreatedOn(new java.sql.Timestamp(utilDate.getTime()));*/
		/*Theme themeObj = null;
		logger.info("before save saveItemList >> "+ConversionToJSON.toJsonString(itemThemeObj));
		if(itemThemeObj.getItemId()>0) {
			logger.info("Inside getItemObject >> ");
			Item itemObj = new Item();
			try {
				itemObj = itemAndThemeDao.getItemDataById(itemThemeObj.getItemId());
				logger.info("After get one itemObj >> "+ConversionToJSON.toJsonString(itemObj));				
			}catch(Exception e) {
				logger.error("Inside catch block >> "+e.getMessage());
			}	
			for(Theme themeOuterObj :itemThemeObj.getThemeList()) {
				for(Theme themeInnerObj:itemObj.getThemeList()) {
					if(themeOuterObj.getThemeId() == themeInnerObj.getThemeId()) {
						logger.info("Inside double loop >> "+themeOuterObj);
						throw new Exception("Already Exist");
					}
				}
			}
		}*/		
		return itemAndThemeDao.saveItemTheme(itemThemeObj);
	}

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@Override
	public List<Theme> getItemThemeData() throws Exception {
		return itemAndThemeDao.getItemThemeData();
	}

	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return Item object
	 * @throws Exception
	 */
	@Override
	public Item getItemDataById(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return Theme object
	 * @throws Exception
	 */
	@Override
	public Theme getThemeDataById(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/*   update the one object of data by id 
	 * 
	 * @param itemObj
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void updateItemTheme(Item itemObj) throws Exception {
		// TODO Auto-generated method stub
		
	}
	/*   update the one object of theme by id 
	 * 
	 * @param themeObj
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void updateTheme(Theme themeObj) throws Exception {
		try {
			itemAndThemeDao.updateTheme(themeObj);
			
		}catch(Exception e) {
			logger.error("error in updateTheme >> "+e.getMessage());
		}
		 
		
	}
	/*   delete the one object of theme by id 
	 * 
	 * @param id
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void deleteTheme(int id) throws Exception {
		try {
			logger.info("deleteTheme id >> "+id);
			itemAndThemeDao.deleteTheme(id);
		}catch(Exception e) {
			logger.error("error in DeleteTheme Service >> "+e.getMessage());
		}
		
	}
	/*   Get the list of Item data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@Override
	public List<Item> getItemData() throws Exception {
		return itemAndThemeDao.getItemData();
	}

}
