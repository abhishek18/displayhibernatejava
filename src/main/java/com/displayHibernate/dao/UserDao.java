package com.displayHibernate.dao;

import com.displayHibernate.model.User;

public interface UserDao {

	public User findByUserName(String username) throws Exception;
}
