package com.displayHibernate.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.displayHibernate.model.Item;
import com.displayHibernate.model.Theme;

@Repository
@Transactional
public class ItemAndThemeDaoImpl implements ItemAndThemeDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	
	private final static Logger logger = LoggerFactory.getLogger(ItemAndThemeDaoImpl.class);
	
	/*   save an List of item and theme data 
	 * 
	 * @param itemThemeObj
	 * @return Long
	 * @throws Exception
	 */
	@Override
	public long saveItemTheme(Theme itemThemeObj) throws Exception {

		logger.info("Inside saveItem  >> ");
		try {
			sessionFactory.getCurrentSession().save(itemThemeObj);
		} catch (Exception e) {
			logger.error("Inside catch saveItem >> "+e.getMessage());
			return 0;
		}
		logger.info("item id from DB is >> "+ itemThemeObj.getThemeId());
		return itemThemeObj.getThemeId();
	}

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@Override
	public List<Theme> getItemThemeData() throws Exception {

		String hql = "FROM Theme";
		logger.info("hql >> "+hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		//logger.info("query >> "+query.toString());
		
		@SuppressWarnings("unchecked")
		List<Theme> itemThemeList = query.list();//session.createQuery(hql).list();
		
		for(Theme itemthemObj : itemThemeList){
            Hibernate.initialize(itemthemObj.getItemList());
        }		
		return itemThemeList;
	}

	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@Override
	public Item getItemDataById(int id) throws Exception {
		return (Item) sessionFactory.getCurrentSession().get(Item.class, id);
	}

	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return Theme object
	 * @throws Exception
	 */
	@Override
	public Theme getThemeDataById(int id) throws Exception {
		// TODO Auto-generated method stub
		return (Theme) sessionFactory.getCurrentSession().get(Theme.class, id);
	}

	/*   update the one object of data by id 
	 * 
	 * @param itemObj
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void updateItemTheme(Item itemObj) throws Exception {
		try {
			sessionFactory.getCurrentSession().update(itemObj);
		} catch(Exception e) {
			logger.error("error in updateDisplay >> "+e.getMessage());
		}
		
	}
	/*   update the one object of theme by id 
	 * 
	 * @param themeObj
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void updateTheme(Theme themeObj) throws Exception {
		try {
			sessionFactory.getCurrentSession().update(themeObj);
		} catch(Exception e) {
			logger.error("error in updateTheme >> "+e.getMessage());
		}		
	}
	/*   delete the one object of theme by id 
	 * 
	 * @param id
	 * @return void
	 * @throws Exception
	 */
	@Override
	public void deleteTheme(int id) throws Exception {
		Theme themeObj = (Theme) sessionFactory.getCurrentSession().byId(Theme.class).load(id);
		sessionFactory.getCurrentSession().delete(themeObj);
		
	}
	/*   Get the list of Item data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@Override
	public List<Item> getItemData() throws Exception {
		String hql = "FROM Item";
		logger.info("hql >> "+hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		//logger.info("query >> "+query.toString());
		
		@SuppressWarnings("unchecked")
		List<Item> itemList = query.list();//session.createQuery(hql).list();
		
		/*for(Item itemObj : itemList){
            Hibernate.initialize(itemObj.getItemList());
        }	*/	
		return itemList;
	}
	
	
	

}
