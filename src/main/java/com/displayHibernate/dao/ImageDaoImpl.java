package com.displayHibernate.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.displayHibernate.model.ImageInfo;

@Repository
@Transactional
public class ImageDaoImpl implements ImageDao{

	@Autowired
	private SessionFactory sessionFactory;

	private final static Logger logger = LoggerFactory.getLogger(ImageDaoImpl.class);
	
	
	/*   save an Object of Image 
	 * 
	 * @param imgObj
	 * @return Long
	 * @throws Exception
	 */
	@Override
	public long saveImage(ImageInfo imgObj) throws Exception {
		logger.info("Inside ImageDaoImpl saveImage >> ");
		try {
			sessionFactory.getCurrentSession().save(imgObj);
		} catch (Exception e) {
			logger.error("Inside catch saveDisplay >> "+e.getMessage());
			return 0;
		}
		logger.info("Display id from DB is >> ", imgObj.getImgId());
		return imgObj.getImgId();
	}

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@Override
	public List<ImageInfo> getImageData() throws Exception {
		
		String hql = "FROM ImageInfo";
		logger.debug("hql >> "+hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		logger.debug("query >> "+query.toString());
		
		List<ImageInfo> imgList = query.list();
		return imgList;
	}


}
