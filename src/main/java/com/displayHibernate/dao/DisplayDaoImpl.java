package com.displayHibernate.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.displayHibernate.model.DisplayInfo;

@Repository
@Transactional
public class DisplayDaoImpl implements DisplayDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	private final static Logger logger = LoggerFactory.getLogger(DisplayDaoImpl.class);
	/*   save an Object of data 
	 * 
	 * @param
	 * @return Long
	 * @throws Exception
	 */
	@Override
	public long saveDisplay(DisplayInfo dispObj) throws Exception {

		logger.debug("Inside DisplayDaoImpl saveDisplay >> ");
		try {
			sessionFactory.getCurrentSession().save(dispObj);
		} catch (Exception e) {
			logger.error("Inside catch saveDisplay >> "+e.getMessage());
			return 0;
		}
		logger.info("Display id from DB is >> ", dispObj.getDisplayId());
		return dispObj.getDisplayId();
	}
	
	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@Override
	public List<DisplayInfo> getDisplayData() throws Exception {
		
		String hql = "FROM DisplayInfo";
		logger.debug("hql >> "+hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		logger.debug("query >> "+query.toString());
		
		List<DisplayInfo> dispList = query.list();
		return dispList;
	}
	
	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@Override
	public DisplayInfo getDisplayDataById(int id) throws Exception {
		return (DisplayInfo) sessionFactory.getCurrentSession().get(DisplayInfo.class, id);
	}
	
	/*   Delete one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@Override
	public void deleteDisplayById(int id) throws Exception {
		DisplayInfo dispObj = (DisplayInfo) sessionFactory.getCurrentSession().byId(DisplayInfo.class).load(id);
		sessionFactory.getCurrentSession().delete(dispObj);
	}

	/*   update the one object of data by id 
	 * 
	 * @param displayObj
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@Override
	public void updateDisplay(DisplayInfo displayObj) throws Exception {
		try {
			sessionFactory.getCurrentSession().update(displayObj);
		} catch(Exception e) {
			logger.error("error in updateDisplay >> "+e.getMessage());
		}
		

	}

	
}
