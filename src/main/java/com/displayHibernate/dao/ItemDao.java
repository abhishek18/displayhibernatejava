package com.displayHibernate.dao;

import java.util.List;

import com.displayHibernate.model.Description;
import com.displayHibernate.model.ImageInfo;
import com.displayHibernate.model.ItemInfo;

public interface ItemDao {
	
	/*   save an List of item data 
	 * 
	 * @param itemObj
	 * @return Long
	 * @throws Exception
	 */
	public long saveItem(ItemInfo itemObj) throws Exception;

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	public List<ItemInfo> getItemData() throws Exception ;
	
	/*   Get the list of data by id
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	public ItemInfo getItemDatabyId(int id) throws Exception ;
	
	/*   save an List of Description data 
	 * 
	 * @param descriptionObj
	 * @return Long
	 * @throws Exception
	 */
	public long saveDescription(Description descriptionObj) throws Exception;
	
	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	public List<ItemInfo> getDescriptionData() throws Exception ;
	
}
