package com.displayHibernate.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.displayHibernate.model.Description;
import com.displayHibernate.model.ItemInfo;
import com.displayHibernate.util.ConversionToJSON;

import org.hibernate.Hibernate;

import org.hibernate.HibernateException;

@Repository
@Transactional
public class ItemDaoImpl /*implements ItemDao*/{

	/*@Autowired
	private SessionFactory sessionFactory;
	/*@Autowired
	private Session session;*

	private final static Logger logger = LoggerFactory.getLogger(ItemDaoImpl.class);
	
	/*   save an List of item data 
	 * 
	 * @param
	 * @return Long
	 * @throws Exception
	 *
	@Override
	public long saveItem(ItemInfo itemObj) throws Exception {
		logger.info("Inside saveItem  >> ");
		try {
			sessionFactory.getCurrentSession().save(itemObj);
		} catch (Exception e) {
			logger.error("Inside catch saveItem >> "+e.getMessage());
			return 0;
		}
		logger.info("item id from DB is >> ", itemObj.getItemId());
		return itemObj.getItemId();
	}

	/*   save an List of Description data 
	 * 
	 * @param descriptionObj
	 * @return Long
	 * @throws Exception
	 *
	@Override
	public long saveDescription(Description descriptionObj) throws Exception {
		logger.info("Inside saveDescription  >> "+ConversionToJSON.toJsonString(descriptionObj));
		try {
			sessionFactory.getCurrentSession().save(descriptionObj);
		} catch (Exception e) {
			logger.error("Inside catch saveItem >> "+e.getMessage());
			return 0;
		}
		logger.info("Description id from DB is >> ", descriptionObj.getDescId());
		return descriptionObj.getDescId();
	}

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 *
	@Override
	public List<ItemInfo> getItemData() throws Exception {
		
		String hql = "FROM ItemInfo";
		logger.info("hql >> "+hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		//logger.info("query >> "+query.toString());
		
		@SuppressWarnings("unchecked")
		List<ItemInfo> itemList = query.list();//session.createQuery(hql).list();
		
		for(ItemInfo itemObj : itemList){
            Hibernate.initialize(itemObj.getDescList());
        }		
		return itemList;
	}

	@Override
	public ItemInfo getItemDatabyId(int id) throws Exception {
		
		/*String hql = "FROM ItemInfo where itemId ="+id;
		logger.info("hql >> "+hql);		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);				
		List<ItemInfo> itemList = query.list();//session.createQuery(hql).list();
					
		return itemList;*
		
		try {
			Session session = sessionFactory.openSession();
			ItemInfo itemInfo2 = (ItemInfo) session.get(ItemInfo.class , 1);
	    	logger.info(ConversionToJSON.toJsonString(itemInfo2));
	    	if(itemInfo2 != null){
	    		itemInfo2.getDescList().forEach(System.out::println);
	    	}
	    	return itemInfo2;
		} catch (HibernateException e) {
			logger.error("Inside catch getItem >> "+e.getMessage());
			return null;
		}
	}

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 *
	@Override
	public List<ItemInfo> getDescriptionData() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}*/
}
