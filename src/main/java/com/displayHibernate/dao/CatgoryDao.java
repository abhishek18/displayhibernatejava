package com.displayHibernate.dao;

import java.util.List;

import com.displayHibernate.model.Category;

public interface CatgoryDao {

	/*   save the List of item and theme data 
	 * 
	 * @param itemThemeObj
	 * @return Long
	 * @throws Exception
	 */
	public long saveCategory(Category categoryObj) throws Exception;

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	public List<Category> getCatgoryData() throws Exception ;
}
