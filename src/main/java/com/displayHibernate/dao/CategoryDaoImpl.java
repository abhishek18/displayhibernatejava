package com.displayHibernate.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.displayHibernate.model.Category;

@Repository
@Transactional
public class CategoryDaoImpl implements CatgoryDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	private final static Logger logger = LoggerFactory.getLogger(CategoryDaoImpl.class);
	
	/*   save the List of category and theme data 
	 * 
	 * @param itemThemeObj
	 * @return Long
	 * @throws Exception
	 */
	@Override
	public long saveCategory(Category categoryObj) throws Exception {
		logger.info("Inside saveCategory  >> ");
		try {
			sessionFactory.getCurrentSession().save(categoryObj);
		} catch (Exception e) {
			logger.error("Inside catch save >> "+e.getMessage());
			return 0;
		}
		logger.info("catg id from DB is >> "+ categoryObj.getCategoryId());
		return categoryObj.getCategoryId();
	}

	@Override
	public List<Category> getCatgoryData() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
