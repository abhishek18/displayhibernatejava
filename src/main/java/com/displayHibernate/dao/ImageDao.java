package com.displayHibernate.dao;

import java.util.List;

import com.displayHibernate.model.ImageInfo;

public interface ImageDao {
	
	/*   save an Object of Image 
	 * 
	 * @param imgObj
	 * @return Long
	 * @throws Exception
	 */
	public long saveImage(ImageInfo imgObj) throws Exception;

	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	public List<ImageInfo> getImageData() throws Exception ;
}
