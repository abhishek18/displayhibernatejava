package com.displayHibernate.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.displayHibernate.model.DisplayInfo;
import com.displayHibernate.model.Item;
import com.displayHibernate.model.ResponseData;
import com.displayHibernate.model.Theme;
import com.displayHibernate.service.ItemWithThemeService;
import com.displayHibernate.util.ConversionToJSON;

@RestController
@RequestMapping("/themeItem")
public class ThemeItemController {
	
	private static Logger logger = LoggerFactory.getLogger(ThemeItemController.class);
	@Autowired
	private ItemWithThemeService itemWithThemeService;
	
	//---------------------Create a theme and Item----------------------------//
	/*   save the List of item and theme data 
	 * 
	 * @param itemThemeObj
	 * @return Long
	 * @throws Exception
	 */
	@PostMapping("/saveThemeItem")
	@ResponseBody  public ResponseEntity<ResponseData> saveThemeItem(@RequestBody Theme themeObj){
		
		logger.info("Inside saveThemeItem controller >> "+ConversionToJSON.toJsonString(themeObj));
		ResponseData responseObj = new ResponseData();
		long id=0;
		try {
			id = itemWithThemeService.saveItem(themeObj);
			responseObj.setResponseStatus("success");
			responseObj.setResponseMsg("Your data has been saved successfully.");
			responseObj.setResponseCode(id);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			responseObj.setResponseStatus("failure");
			responseObj.setResponseMsg("Sorry!!The data can not be saved.");
			responseObj.setResponseCode(id);
		}
		if(id > 0 ) {
			return new ResponseEntity<>(responseObj,HttpStatus.CREATED);
		}else {
			return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
		}
	}
	//--------------------End Of Create a theme and Item------------------------//

	//---------------------Fetch a theme and Item----------------------------//
	/*   Get the list of data 
	 * 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@GetMapping("/getThemeItem")
	@ResponseBody  public ResponseEntity<ResponseData> getThemeItem(){
		ResponseData responseObj = new ResponseData();
		List<Theme> themeList= null;
		try {
			themeList = itemWithThemeService.getItemThemeData();
			if(themeList != null && !themeList.isEmpty()) {
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("Your data has been fetched successfully.");
				responseObj.setObj(themeList);
				return new ResponseEntity<>(responseObj,HttpStatus.OK);
				
			}else {
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("No data found.");
				responseObj.setObj(themeList);
				return new ResponseEntity<>(responseObj,HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseObj.setResponseStatus("failure");
			responseObj.setResponseMsg("Sorry!!"+e.getMessage());
			return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
		}
		
	}
	//---------------------End Of Fetch a theme and Item-----------------------//
	
	
	//---------------------Update a theme and Item-----------------------//
	
	/*   update the one object of theme by id 
	 * 
	 * @param themeObj
	 * @return void
	 * @throws Exception
	 */
	@PutMapping("/updateTheme")
	@ResponseBody  public ResponseEntity<ResponseData> updateThemeItem(@RequestBody Theme themeObj){
		ResponseData responseObj = new ResponseData();
		
		try {
			itemWithThemeService.updateTheme(themeObj);
			responseObj.setResponseStatus("success");
			responseObj.setResponseMsg("Your data has been updated successfully.");
			responseObj.setObj(themeObj);
			return new ResponseEntity<>(responseObj,HttpStatus.OK);
		}catch (Exception e) {
			logger.error(e.getMessage());
			responseObj.setResponseStatus("failure");
			responseObj.setResponseMsg("Sorry!!"+e.getMessage());
			return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
		}
	}
	//---------------------End Of Update a theme and Item-----------------------//
	
	//------------------- delete a theme and Item -------------------------------//
	/*   delete the one object of theme by id 
	 * 
	 * @param id
	 * @return void
	 * @throws Exception
	 */
	//---------------------End Of delete a theme and Item-----------------------//
	
	//---------------------Fetch Item List----------------------------//
		/*   Get the list of data 
		 * 
		 * @param 
		 * @return List
		 * @throws Exception
		 */
		@GetMapping("/getAllItem")
		@ResponseBody  public ResponseEntity<ResponseData> getAllItem(){
			ResponseData responseObj = new ResponseData();
			List<Item> itemList= null;
			try {
				itemList = itemWithThemeService.getItemData();
				if(itemList != null && !itemList.isEmpty()) {
					responseObj.setResponseStatus("success");
					responseObj.setResponseMsg("Your data has been fetched successfully.");
					responseObj.setObj(itemList);
					return new ResponseEntity<>(responseObj,HttpStatus.OK);
					
				}else {
					responseObj.setResponseStatus("success");
					responseObj.setResponseMsg("No data found.");
					responseObj.setObj(itemList);
					return new ResponseEntity<>(responseObj,HttpStatus.NO_CONTENT);
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
				responseObj.setResponseStatus("failure");
				responseObj.setResponseMsg("Sorry!!"+e.getMessage());
				return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
			}			
		}
		//---------------------End Of Fetch Item List-----------------------//
	
}
