package com.displayHibernate.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.displayHibernate.model.DisplayInfo;
import com.displayHibernate.model.ResponseData;
import com.displayHibernate.service.DisplayService;
import com.displayHibernate.util.ConversionToJSON;



@RestController
@RequestMapping("/display")
public class DisplayController {

	private static Logger logger = LoggerFactory.getLogger(DisplayController.class);
	@Autowired
	private DisplayService displayService;
	//Gdx.net.openURI(String URI);
	/*Websocket socket = new Websocket("ws://hostname:port/path");
	
	//socket.
	socket.addListener(new WebsocketListener() {

	    @Override
	    public void onClose(CloseEvent event) {
		    // do something on close
	    }

	    @Override
	    public void onMessage(String msg) {
	        // a message is received
	    }

	    @Override
	    public void onOpen() {
	    	socket.open();
	    }
	});*/
	
	
	//-------------------Create a Text--------------------------------------------------------//
	/*   save an Object of data 
	 * 
	 * @param dispObj
	 * @return Long
	 * @throws Exception
	 */
	@PostMapping("/saveDisplay")
	public ResponseEntity<ResponseData> create(@RequestBody DisplayInfo dispObj) {
		logger.info("Inside controller >> "+ConversionToJSON.toJsonString(dispObj));
		ResponseData responseObj = new ResponseData();
		long id=0;
		try {
			id = displayService.saveDisplay(dispObj);
			responseObj.setResponseStatus("success");
			responseObj.setResponseMsg("Your data has been saved successfully.");
			responseObj.setResponseCode(id);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			responseObj.setResponseStatus("failure");
			responseObj.setResponseMsg("Sorry!!The data can not be saved.");
			responseObj.setResponseCode(id);
		}
		if(id > 0 ) {
			return new ResponseEntity<>(responseObj,HttpStatus.CREATED);
		}else {
			return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
		}
	}
	//-------------------Retrieve All data---------------------------------------------------
	/*   Get the list of data 
	 * @param 
	 * @return List
	 * @throws Exception
	 */
	@GetMapping("/getAllDisplay")
	public ResponseEntity<ResponseData> getAllDisplay() {
		ResponseData responseObj = new ResponseData();
		List<DisplayInfo> dispList= null;
		try {
			dispList = displayService.getDisplayData();
			if(dispList != null && !dispList.isEmpty()) {
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("Your data has been fetched successfully.");
				responseObj.setObj(dispList);
				return new ResponseEntity<>(responseObj,HttpStatus.OK);
				
			}else {
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("No data found.");
				responseObj.setObj(dispList);
				return new ResponseEntity<>(responseObj,HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseObj.setResponseStatus("failure");
			responseObj.setResponseMsg("Sorry!!"+e.getMessage());
			return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
		}
		
		
	}
	//-------------------Retrieve Single Object-------------------------------------------------
	/*   Get the one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@GetMapping("/getDisplay/{id}")
	public ResponseEntity<ResponseData> getOneDisplay(@PathVariable("id") int id) {

		ResponseData responseObj = new ResponseData();
		DisplayInfo displayObj = new DisplayInfo();
		try {
			displayObj = displayService.getDisplayDataById(id);
			if(displayObj == null) {
				logger.info("Display with id {} ", id ,"not found {}");
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("No data found.");
				responseObj.setObj(displayObj);
				
				return new ResponseEntity<>(responseObj,HttpStatus.NOT_FOUND);
			} else {
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("Your data has been fetched successfully.");
				responseObj.setObj(displayObj);
				responseObj.setResponseCode(displayObj.getDisplayId());
				return new ResponseEntity<>(responseObj,HttpStatus.OK);
			}						
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseObj.setResponseStatus("failure");
			responseObj.setResponseMsg("Sorry!!"+e.getMessage());
			return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
		}		
	}
	//------------------- Delete a User ------------------------------------------------------//
	/*   Delete one object of data by id 
	 * 
	 * @param int id
	 * @return DisplayInfo object
	 * @throws Exception
	 */
	@DeleteMapping("/deleteDisplay/{id}")
	public ResponseEntity<ResponseData> deleteDisplayById(@PathVariable("id") int id) {
		ResponseData responseObj = new ResponseData();
		boolean checkDelete = false;
		DisplayInfo displayObj = new DisplayInfo();
		try {
			checkDelete = displayService.deleteDisplayById(id);
			logger.info("Display with id {} ", id ,"whether deleted {}"+checkDelete);
			if(checkDelete) {
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("Your data has been deleted successfully.");
				return new ResponseEntity<>(responseObj,HttpStatus.OK);
			}else {
				responseObj.setResponseStatus("failure");
				responseObj.setResponseMsg("Sorry!!Your data can not be deleted.");
				return new ResponseEntity<>(responseObj,HttpStatus.METHOD_FAILURE);
			}
		}catch (Exception e) {
			logger.error(e.getMessage());
			responseObj.setResponseStatus("failure");
			responseObj.setResponseMsg("Sorry!!"+e.getMessage());
			return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
		}		
	}
	
	//------------------- Update a User ------------------------------------------------------
	/*   update the one object of data by id 
	 * 
	 * @param displayObj
	 * @return DisplayInfo object
	 * @throws Exception
	 */
		@PutMapping("/updateDisplay/{id}")
		public ResponseEntity<ResponseData> updateDisplay(@PathVariable("id") int id, @RequestBody DisplayInfo displayObj ) {
			ResponseData responseObj = new ResponseData();
			try {
				displayService.updateDisplay(displayObj);
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("Your data has been updated successfully.");
				return new ResponseEntity<>(responseObj,HttpStatus.OK);
			}catch (Exception e) {
				responseObj.setResponseStatus("failure");
				responseObj.setResponseMsg("Sorry!!"+e.getMessage());
				return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
			}
		}
}
