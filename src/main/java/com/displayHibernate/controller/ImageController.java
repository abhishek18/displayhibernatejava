package com.displayHibernate.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.displayHibernate.model.ImageInfo;
import com.displayHibernate.model.ResponseData;
import com.displayHibernate.service.ImageService;
import com.displayHibernate.util.ConversionToJSON;

@RestController
@RequestMapping("/image")
public class ImageController {
	
	private static Logger logger = LoggerFactory.getLogger(ImageController.class);
	@Autowired
	private ImageService imageService;
	
	//-------------------Create a Text--------------------------------------------------------//
		/*   save an Object of data 
		 * 
		 * @param dispObj
		 * @return Long
		 * @throws Exception
		 */
		@PostMapping("/saveImage")
		public ResponseEntity<ResponseData> createImage(@RequestBody ImageInfo imgObj) {
			logger.info("Inside controller >> "+ConversionToJSON.toJsonString(imgObj));
			ResponseData responseObj = new ResponseData();
			long id=0;
			try {
				id = imageService.saveImage(imgObj);
				responseObj.setResponseStatus("success");
				responseObj.setResponseMsg("Your data has been saved successfully.");
				responseObj.setResponseCode(id);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
				responseObj.setResponseStatus("failure");
				responseObj.setResponseMsg("Sorry!!The data can not be saved.");
				responseObj.setResponseCode(id);
			}
			if(id > 0 ) {
				return new ResponseEntity<>(responseObj,HttpStatus.CREATED);
			}else {
				return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
			}
		}
		//-------------------Retrieve All data---------------------------------------------------
		/*   Get the list of data 
		 * 
		 * @param 
		 * @return List
		 * @throws Exception
		 */
		@GetMapping("/getAllImage")
		public ResponseEntity<ResponseData> getAllDisplay() {
			logger.info("Inside getAllDisplay >> ");
			ResponseData responseObj = new ResponseData();
			List<ImageInfo> imgList= null;
			try {
				imgList = imageService.getImageData();
				if(null != imgList  && !imgList.isEmpty()) {
					responseObj.setResponseStatus("success");
					responseObj.setResponseMsg("Your data has been fetched successfully.");
					responseObj.setObj(imgList);
					return new ResponseEntity<>(responseObj,HttpStatus.OK);
					
				}else {
					responseObj.setResponseStatus("success");
					responseObj.setResponseMsg("No data found.");
					responseObj.setObj(imgList);
					return new ResponseEntity<>(responseObj,HttpStatus.NO_CONTENT);
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
				responseObj.setResponseStatus("failure");
				responseObj.setResponseMsg("Sorry!!"+e.getMessage());
				return new ResponseEntity<>(responseObj,HttpStatus.CONFLICT);
			}			
		}

}
