package com.displayHibernate.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ConversionToJSON {
	private static final Logger logger = LoggerFactory.getLogger(ConversionToJSON.class);
	
	private ConversionToJSON() {}	
	
	public static String toJsonString(Object object) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString  = null;
		//Object to JSON in String
		try {
			jsonInString = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			logger.info(e.getMessage());
		}
		logger.info("Object in json string {} ", jsonInString);
		return jsonInString;
	}

}
