package com.displayHibernate.service.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.displayHibernate.config.DisplayConfig;
import com.displayHibernate.model.DisplayInfo;
import com.displayHibernate.service.DisplayService;
import com.displayHibernate.util.ConversionToJSON;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DisplayConfig.class })
public class DisplayServiceImplTest {

	@Autowired
	private DisplayService displayService;
	private static final Logger logger = LoggerFactory.getLogger(DisplayServiceImplTest.class);
	
	@Test
	public void saveDisplayObject() {
		
		logger.info("Inside saveDisplayObject Service >> ");
		DisplayInfo displayObj = new DisplayInfo();
		
		//displayObj.setDisplayId(1);
		displayObj.setDisplayText("Y");
		displayObj.setTextInfo("Fourth Quarter 2018 Earnings Release, Conference Call and Webcast.");
		logger.info("Inside service >> "+ConversionToJSON.toJsonString(displayObj));
		
		try {
			long createdid = displayService.saveDisplay(displayObj);
			logger.info("After save DisplayObject Service >> "+createdid);
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}
		
	}
}
