package com.displayHibernate.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.displayHibernate.config.DisplayConfig;
import com.displayHibernate.model.Item;
import com.displayHibernate.model.Theme;
import com.displayHibernate.service.DisplayService;
import com.displayHibernate.service.ItemWithThemeService;
import com.displayHibernate.util.ConversionToJSON;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DisplayConfig.class })
public class ItemWithThemeServiceImplTest {

	@Autowired
	private ItemWithThemeService itemWithThemeService;
	
	private static final Logger logger = LoggerFactory.getLogger(ItemWithThemeServiceImplTest.class);
	
	
	//@Test
	public void saveItemThemeListManyToMany() {
		
		logger.info("Inside saveItemListManyToMany  >> ");
		Item itemObj1 = new Item();
		Item itemObj2 = new Item();
		Theme themeObj1 = new Theme();
		
		List<Item> itemList = new ArrayList<>();
		 
		try {
			itemObj1.setItemName("Salwar Suit");
			itemObj2.setItemName("Kanchipuram");
			
			//themeObj1.setThemeDesc("hopping");
			themeObj1.setThemeName("Dextrus");
			themeObj1.setThemeDesc(" Dextrus hand is designed with the user in mind.");
			//qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
			//java.sql.Date modith=java.sql.Date.valueOf("2019-04-11 ");
			java.util.Date utilDate = new java.util.Date();
			//itemObj.setUploadDate(new java.sql.Timestamp(utilDate.getTime()));
			//itemObj.setUploadDate(new java.sql.Date(utilDate.getTime()));
			//itemObj.setItemName("Hollywood");
			//itemObj.setUploadDate(modith);
			/*for(Description descObj:) {
				new Description()
			}*/	
			itemList.add(itemObj1);
			itemList.add(itemObj2);
			themeObj1.setItemList(itemList);
			//itemList.add(itemObj);
			logger.info("before save saveItemList >> "+ConversionToJSON.toJsonString(themeObj1));
			//logger.info("size :: "+itemList.size());
			
			long dbId = itemWithThemeService.saveItem(themeObj1);
			logger.info("after save saveItemList >> "+dbId);
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}
	}
	
	@Test
	public void getItemAll() {
		logger.info("Inside getItemAll list >> ");
		List<Item> itemList = new ArrayList<>();
		try {
			itemList = itemWithThemeService.getItemData();
			logger.info("After get itemList >> "+((itemList==null)));
			
			logger.info("After get itemList :: "+ConversionToJSON.toJsonString(itemList));
			logger.info("After get itemList Size :: "+itemList.size());
		}catch(Exception e) {
			logger.error("Inside catch block itemlist >> "+e.getMessage());
		}	
	}	
}
