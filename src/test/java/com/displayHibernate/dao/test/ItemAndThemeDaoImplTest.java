package com.displayHibernate.dao.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.displayHibernate.config.DisplayConfig;
import com.displayHibernate.dao.CatgoryDao;
import com.displayHibernate.dao.ItemAndThemeDao;
import com.displayHibernate.model.Category;
import com.displayHibernate.model.DisplayInfo;
import com.displayHibernate.model.Item;
import com.displayHibernate.model.ItemInfo;
import com.displayHibernate.model.SubCategory;
import com.displayHibernate.model.Theme;
import com.displayHibernate.util.ConversionToJSON;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DisplayConfig.class })
public class ItemAndThemeDaoImplTest {
	
	@Autowired
	private ItemAndThemeDao itemAndThemeDao;
	@Autowired
	private CatgoryDao catgoryDao;
	
	private static final Logger logger = LoggerFactory.getLogger(ItemAndThemeDaoImplTest.class);
	
	//@Test
	public void saveItemThemeListManyToMany() {
		
		logger.info("Inside saveItemListManyToMany  >> ");
		Item itemObj1 = new Item();
		Item itemObj2 = new Item();
		Theme themeObj1 = new Theme();
		
		List<Item> itemList = new ArrayList<>();
		
		try {
			itemObj1.setItemName("Red Saree");
			itemObj2.setItemName("Sneha Saree");
			
			//themeObj1.setThemeDesc("hopping");
			themeObj1.setThemeDesc("Theme 214");
			//qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
			//java.sql.Date modith=java.sql.Date.valueOf("2019-04-11 ");
			java.util.Date utilDate = new java.util.Date();
			//itemObj.setUploadDate(new java.sql.Timestamp(utilDate.getTime()));
			//itemObj.setUploadDate(new java.sql.Date(utilDate.getTime()));
			//itemObj.setItemName("Hollywood");
			//itemObj.setUploadDate(modith);
			/*for(Description descObj:) {
				new Description()
			}*/	
			itemList.add(itemObj1);
			itemList.add(itemObj2);
			themeObj1.setItemList(itemList);
			//itemList.add(itemObj);
			logger.info("before save saveItemList >> "+ConversionToJSON.toJsonString(themeObj1));
			//logger.info("size :: "+itemList.size());
			
			long dbId = itemAndThemeDao.saveItemTheme(themeObj1);
			logger.info("after save saveItemList >> "+dbId);
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}
	}
	
	//@Test
	public void getItemThemeListManyToMany() {
		logger.info("Inside getItemThemeListManyToMany list >> ");

		List<Theme> themeList = new ArrayList<>();
		//Item itemObj = new Item();
		logger.info("size :: "+themeList.size());
		
		try {
			//itemObj = itemDao.getItemDatabyId(1);
			themeList = itemAndThemeDao.getItemThemeData();
			logger.info("After get itemList >> "+((themeList==null)));
			/*for(ItemInfo itemOb: itemList) {
				logger.info("item :: "+ConversionToJSON.toJsonString(itemOb));
			}*/
			
			logger.info("After get itemList :: "+ConversionToJSON.toJsonString(themeList));
			logger.info("After get itemList Size :: "+themeList.size());
		}catch(Exception e) {
			logger.error("Inside catch block itemlist >> "+e.getMessage());
		}	
	}
	
	//@Test
	public void getDataById() {
		
		logger.info("Inside getItemObject >> ");
		Item itemObj = new Item();
		try {
			itemObj = itemAndThemeDao.getItemDataById(448963);
			logger.info("After get one itemObj >> "+ConversionToJSON.toJsonString(itemObj));
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}	
	}
	//@Test
	public void getThemeById() {
		
		logger.info("Inside getThemeObject >> ");
		Theme themeObj = new Theme();
		try {
			themeObj = itemAndThemeDao.getThemeDataById(11);
			logger.info("After get one themeObj >> "+ConversionToJSON.toJsonString(themeObj));
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}	
	}
	
	//@Test
	public void updateDataById() {
		logger.info("Inside updateDataById >> ");
		Item itemObj = new Item();
		Theme themeObj1 = new Theme();
		//Theme themeObj2 = new Theme();
		List<Theme> themeList = new ArrayList<>();
		java.sql.Date modith=java.sql.Date.valueOf("2019-04-11");
		itemObj.setItemId(448963);
		//itemObj.setItemName("Saree");
		//itemObj.setUploadDate(modith);
		
		themeObj1.setThemeId(11);
		themeObj1.setThemeDesc("Durga Puja");
		themeList.add(themeObj1);
		itemObj.setThemeList(themeList);
		logger.info("Inside UpdateItemObject >> "+ConversionToJSON.toJsonString(itemObj));
		try {
			itemAndThemeDao.updateItemTheme(itemObj);

			logger.info("After update ItemObject >> ");
			logger.info("After update ItemObject TEST >> "+ConversionToJSON.toJsonString(itemObj));
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}	
	}
	
	//@Test
	public void saveCategoryListOneToMany() {
		Category catgObj = new Category();
		SubCategory subcatgObj1 = new SubCategory();
		SubCategory subcatgObj2 = new SubCategory();
		List<SubCategory> subCatgList = new ArrayList<>(); 
		Theme themeObj1 = new Theme();
		Theme themeObj2 = new Theme();
		List<Theme> themLt1 = new ArrayList<>();
		List<Theme> themLt2 = new ArrayList<>();
		
		try {
			//catgObj.setCategoryId(1);
			catgObj.setCategoryDesc("CatgoryDesc11");
			
			themeObj1.setThemeDesc("themeDesc 101");
			themeObj2.setThemeDesc("themeDesc 202");
			themLt1.add(themeObj1);
			themLt2.add(themeObj2);
			//subcatgObj1.setSubcategoryId(1);
			subcatgObj1.setSubcategoryDesc("subcategoryDesc1");
			subcatgObj1.setThemeSubCList(themLt1);
			
			//subcatgObj2.setSubcategoryId(2);
			subcatgObj2.setSubcategoryDesc("subcategoryDesc22");
			subcatgObj2.setThemeSubCList(themLt2);
			subCatgList.add(subcatgObj1);
			subCatgList.add(subcatgObj2);
			
			
			
			catgObj.setSubCatgList(subCatgList);
			
			logger.info("before save saveCategoryListOneToMany >> "+ConversionToJSON.toJsonString(catgObj));
			
			long dbId = catgoryDao.saveCategory(catgObj);
			logger.info("after save saveItemList >> "+dbId);
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}
	}
	//@Test
	public void updateThemeById() {
		logger.info("Inside updateThemeById  >> ");
		Item itemObj1 = new Item();
		Item itemObj2 = new Item();
		Theme themeObj1 = new Theme();
		
		List<Item> itemList = new ArrayList<>();
		
		try {
			itemObj1.setItemId(12);
			itemObj1.setItemName("gulistan");
			itemObj2.setItemId(13);
			itemObj2.setItemName("Saraghari");
			
			//themeObj1.setThemeDesc("hopping");
			themeObj1.setThemeId(6);
			themeObj1.setThemeName("Galib");
			themeObj1.setThemeDesc("Rayan");
			//qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
			//java.sql.Date modith=java.sql.Date.valueOf("2019-04-11 ");
			java.util.Date utilDate = new java.util.Date();
			//itemObj.setUploadDate(new java.sql.Timestamp(utilDate.getTime()));
			//itemObj.setUploadDate(new java.sql.Date(utilDate.getTime()));
			//itemObj.setItemName("Hollywood");
			//itemObj.setUploadDate(modith);
			/*for(Description descObj:) {
				new Description()
			}*/	
			itemList.add(itemObj1);
			itemList.add(itemObj2);
			themeObj1.setItemList(itemList);
			//itemList.add(itemObj);
			logger.info("before save saveItemList >> "+ConversionToJSON.toJsonString(themeObj1));
			//logger.info("size :: "+itemList.size());
			
			 itemAndThemeDao.updateTheme(themeObj1);
			logger.info("after save saveItemList >> ");
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}
	}

	@Test
	public void getItemAll() {
		logger.info("Inside getItemAll list >> ");
		List<Item> itemList = new ArrayList<>();
		try {
			itemList = itemAndThemeDao.getItemData();
			logger.info("After get itemList >> "+((itemList==null)));
			
			logger.info("After get itemList :: "+ConversionToJSON.toJsonString(itemList));
			logger.info("After get itemList Size :: "+itemList.size());
		}catch(Exception e) {
			logger.error("Inside catch block itemlist >> "+e.getMessage());
		}	
	}	
}


