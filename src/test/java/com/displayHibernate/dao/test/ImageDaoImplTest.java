package com.displayHibernate.dao.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.displayHibernate.config.DisplayConfig;
import com.displayHibernate.dao.ImageDao;
import com.displayHibernate.model.DisplayInfo;
import com.displayHibernate.model.ImageInfo;
import com.displayHibernate.util.ConversionToJSON;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DisplayConfig.class })
public class ImageDaoImplTest {

	@Autowired
	private ImageDao imageDao;
	private static final Logger logger = LoggerFactory.getLogger(ImageDaoImplTest.class);
	
	@Test
	public void saveImageObject() {
		logger.debug("Inside saveImageObject >> ");
		
		ImageInfo imgObj = new ImageInfo();
		java.sql.Timestamp crtd=java.sql.Timestamp.valueOf("2019-03-04 16:32:34");
		File file = new File("E:\\panorama_of_NYC.jpg");
        byte[] bFile = new byte[(int) file.length()];
        try {
        	
        	FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
            
            imgObj.setImgTxt("Y");
    		imgObj.setImgFile(bFile);
    		imgObj.setImgInfo("At its core is Manhattan, a densely populated borough that�s among the world�s major commercial.");
    		imgObj.setDefunct("N");
    		imgObj.setCreatedBy("Abhishek");
    		imgObj.setCreatedOn(crtd);
    		imgObj.setFlag("I");
    		logger.info("Inside save imgObj >> "+ConversionToJSON.toJsonString(imgObj));
    		
    		long createdid = imageDao.saveImage(imgObj);
			logger.info("After save createdid >> "+createdid);
			logger.debug(" Debug");
			
        }catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}				
	}
	
	//@Test
	public void getImageList() {
		logger.info("Inside getImageList list >> ");
		//ImageInfo imgObj = new ImageInfo();
		List<ImageInfo> imgList = null;
		
		try {
			imgList = imageDao.getImageData();
			logger.info("After get DisplayObject >> "+ConversionToJSON.toJsonString(imgList));
			logger.info("size :: "+imgList.size());
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}	
	}
}
