package com.displayHibernate.dao.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.displayHibernate.config.DisplayConfig;
import com.displayHibernate.dao.DisplayDao;
import com.displayHibernate.model.DisplayInfo;
import com.displayHibernate.util.ConversionToJSON;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DisplayConfig.class })
public class DisplayDaoImplTest {

	@Autowired
	private DisplayDao displayDao;
	private static final Logger logger = LoggerFactory.getLogger(DisplayDaoImplTest.class);
	
	//@Test
	public void saveDisplayObject() {
		logger.debug("Inside saveDisplayObject >> ");
		DisplayInfo displayObj = new DisplayInfo();
		java.sql.Timestamp birth=java.sql.Timestamp.valueOf("2019-01-12 10:26:34");
		//displayObj.setDisplayId(1);
		displayObj.setDisplayText("Y");
		displayObj.setTextInfo("American multinational corporation headquartered in Oak Brook, Illinois, a suburb of Chicago.");
		displayObj.setCreatedBy("Abhishek");
		displayObj.setCreatedOn(birth);
		logger.info("Inside saveDisplayObject >> "+ConversionToJSON.toJsonString(displayObj));
		
		try {
			long createdid = displayDao.saveDisplay(displayObj);
			logger.info("After save DisplayObject >> "+createdid);
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}
		
	}
	//@Test
	public void getData() {
		
		logger.info("Inside getDisplayObject >> ");
		List<DisplayInfo> dispList = null;
		
		try {
			dispList = displayDao.getDisplayData();
			logger.info("After get DisplayObject >> "+ConversionToJSON.toJsonString(dispList));
			logger.info("size :: "+dispList.size());
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}	
	}
	//@Test
	public void getDataById() {
		
		logger.info("Inside getDisplayObject >> ");
		DisplayInfo displayObj = new DisplayInfo();
		try {
			displayObj = displayDao.getDisplayDataById(13);
			logger.info("After get one DisplayObject >> "+ConversionToJSON.toJsonString(displayObj));
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}	
	}
	//@Test
	public void deletDataById() {
		logger.info("Inside deletDataById >> ");
		//DisplayInfo displayObj = new DisplayInfo();
		try {
			displayDao.deleteDisplayById(6);
			//displayDao.getDisplayData();
			logger.info("After delete DisplayObject >> ");
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}	
	}
	//@Test
	public void updateDataById() {
		logger.info("Inside updateDataById >> ");
		DisplayInfo displayObj = new DisplayInfo();
		java.sql.Timestamp modith=java.sql.Timestamp.valueOf("2019-01-15 11:38:34");
		displayObj.setDisplayId(1);
		displayObj.setDisplayText("Y");
		displayObj.setTextInfo("Malibu is a city west of Los Angeles, California. It�s known for its celebrity homes and beaches, including wide and sandy Zuma Beach.");
		displayObj.setModifiedBy("Abhishek");
		displayObj.setModifiedOn(modith);
		displayObj.setCreatedBy("Abhishek");
		logger.info("Inside saveDisplayObject >> "+ConversionToJSON.toJsonString(displayObj));
		try {
			displayDao.updateDisplay(displayObj);;
			//displayDao.getDisplayData();
			logger.info("After update DisplayObject >> ");
			logger.info("After update DisplayObject TEST >> "+ConversionToJSON.toJsonString(displayObj));
			
		}catch(Exception e) {
			logger.error("Inside catch block >> "+e.getMessage());
		}	
	}
}
